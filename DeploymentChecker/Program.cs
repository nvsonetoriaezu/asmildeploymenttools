﻿using DeploymentUtility;
using System;

namespace DeploymentChecker
{
    class Program
    {
        private static char[] separatorArr = new char[] { '/' };

        /// <summary>
        /// 指定日時以降にデプロイされたファイルがあるか判定します。
        /// </summary>
        /// <param name="args">コマンドライン引数</param>
        /// <returns>0=デプロイ成功, -1=エラー</returns>
        static int Main(string[] args)
        {
            if (args.Length < (int)ArgumentLength.WithoutLogFilePath)
            {
                return (int)ToolResult.Error;
            }

            string rootDirectoryPath = "";
            DateTime startDateTime;
            string[] searchFileNameArr;
            string logFilePath = "";
            bool deployed = false;

            try
            {
                //引数は検索ディレクトリパス, 基準日時文字列, 検索対象ファイル名, ログファイルパスの順
                rootDirectoryPath = args[0];
                startDateTime = DateTime.Parse(args[1]);
                searchFileNameArr = args[2].Split(separatorArr);
                if ((int)ArgumentLength.WithLogFilePath <= args.Length)
                {
                    logFilePath = args[3];
                }

                deployed = FileManager.FindDeployedFiles(rootDirectoryPath, startDateTime, searchFileNameArr);
                if (!deployed)
                {
                    string msg = string.Format("Deployed files not found. Dir={0}, StartDateTime={1}, SearchFiles={2}", args[0], args[1], args[2]);
                    throw new System.IO.FileNotFoundException(msg);
                }
            }
            catch (Exception ex)
            {
                string caption = "DeploymentChecker Error";
                Console.WriteLine(caption);
                Console.WriteLine(ex.Message);
                if (!string.IsNullOrWhiteSpace(logFilePath))
                {
                    DeploymentLogger.Logger.WriteException(logFilePath, ex, caption);
                }
                return (int)ToolResult.Error;
            }

            return (int)ToolResult.Ok;
        }
    }
}
