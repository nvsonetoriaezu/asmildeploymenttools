﻿using System;
using System.IO;
using System.Linq;

namespace DeploymentChecker
{
    /// <summary>
    /// ファイルの管理クラスです。
    /// </summary>
    public class FileManager
    {
        /// <summary>
        /// 指定日時以降にデプロイされたファイルを検索します。
        /// </summary>
        /// <param name="rootDirectoryPath">検索対象ディレクトリパス</param>
        /// <param name="startDateTime">開始日時</param>
        /// <param name="searchFileNameArr">検索するファイル名の配列</param>
        /// <returns>true=デプロイ済みファイルあり, false=デプロイ済みファイルなし</returns>
        public static bool FindDeployedFiles(string rootDirectoryPath, DateTime startDateTime, string[] searchFileNameArr)
        {
            bool result = false;
            DirectoryInfo rootDirectory = new DirectoryInfo(rootDirectoryPath);
            // ディレクトリのサブディレクトリの中を検索する
            foreach (DirectoryInfo directory in rootDirectory.EnumerateDirectories())
            {
                if (startDateTime <= directory.CreationTime)
                {
                    result = searchFileNameArr.All(arrItem => directory.EnumerateFiles().Any(file => file.Name.Contains(arrItem)));
                    if (result)
                    {
                        break;
                    }
                }
            }

            // ディレクトリの直下のファイルを検索する
            if (!result)
            {
                result = searchFileNameArr.All(arrItem => rootDirectory.EnumerateFiles().Any(file => file.Name.Contains(arrItem) && (startDateTime <= file.CreationTime)));
            }

            return result;
        }
    }
}
