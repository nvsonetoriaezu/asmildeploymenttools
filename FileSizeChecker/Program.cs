﻿using DeploymentUtility;
using System;

namespace FileSizeChecker
{
    class Program
    {
        static int Main(string[] args)
        {
            if (args.Length < (int)ArgumentLength.WithoutLogFilePath)
            {
                return (int)ToolResult.Error;
            }

            string srcFilePath = "";
            string logFilePath = "";
            
            try
            {
                //引数はファイルパス, ログファイルパスの順
                srcFilePath = args[0];
                if ((int)ArgumentLength.WithLogFilePath <= args.Length)
                {
                    logFilePath = args[1];
                }

                if (FileManager.HasFileData(srcFilePath))
                {
                    return (int)ToolResult.Ok;
                }
                else
                {
                    return (int)ToolResult.NotOk;
                }

            }
            catch (Exception ex)
            {
                string caption = "FileSizeChecker Error";
                Console.WriteLine(caption);
                Console.WriteLine(ex.Message);
                if (!string.IsNullOrWhiteSpace(logFilePath))
                {
                    DeploymentLogger.Logger.WriteException(logFilePath, ex, caption);
                }
                return (int)ToolResult.Error;
            }
        }
    }
}
