﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileSizeChecker
{
    public class FileManager
    {
        /// <summary>
        /// ファイルがデータを保持しているか判定します。
        /// </summary>
        /// <param name="filePath">ファイルパス</param>
        /// <returns>true=データが1バイト以上ある, false=データが0バイト</returns>
        public static bool HasFileData(string filePath)
        {
            FileInfo fileInfo = new FileInfo(filePath);
            if (!fileInfo.Exists)
            {
                throw new FileNotFoundException(filePath + " is not found.");
            }

            bool result = (0 < fileInfo.Length);
            return result;
        }
    }
}
