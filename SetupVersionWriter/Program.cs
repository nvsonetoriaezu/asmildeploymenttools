﻿using DeploymentUtility;
using System;

namespace SetupVersionWriter
{
    class Program
    {
        /// <summary>
        /// プロジェクトファイルのバージョン番号を更新します。
        /// </summary>
        /// <param name="args">コマンドライン引数</param>
        /// <returns>0=成功, -1=エラー</returns>
        static int Main(string[] args)
        {
            string projectFilePath = "";
            string assemblyInfoFilePath1 = "";
            string assemblyInfoFilePath2 = "";
            string logFilePath = "";

            try
            {
                // vdproj,assemblyinfo,assemblyinfoを要求する。あとエラーログのファイルパス
                if (args.Length < (int)ArgumentLength.WithoutLogFilePath)
                {
                    return (int)ToolResult.Error;
                }

                projectFilePath = args[0];
                assemblyInfoFilePath1 = args[1];
                assemblyInfoFilePath2 = args[2];
                if ((int)ArgumentLength.WithLogFilePath <= args.Length)
                {
                    logFilePath = args[3];
                }

                VersionData newVersion = VersionDataManager.UpdateBuild(VersionFileManager.ReadVersion(assemblyInfoFilePath1, FileType.AssemblyInfo));
                VersionFileManager.WriteVersion(projectFilePath, FileType.DeploymentProject, newVersion);
                VersionFileManager.WriteVersion(assemblyInfoFilePath1, FileType.AssemblyInfo, newVersion);
                VersionFileManager.WriteVersion(assemblyInfoFilePath2, FileType.AssemblyInfo, newVersion);
            }
            catch (Exception ex)
            {
                string caption = string.Format(
                    "SetupVersionWriter Error : ProjectFile={0}, AssemblyInfo1={1}, AssemblyInfo2={2}",
                    projectFilePath,
                    assemblyInfoFilePath1,
                    assemblyInfoFilePath2);
                Console.WriteLine(caption);
                Console.WriteLine(ex.Message);
                if (!string.IsNullOrWhiteSpace(logFilePath))
                {
                    DeploymentLogger.Logger.WriteException(logFilePath, ex, caption);
                }
                return (int)ToolResult.Error;
            }

            return (int)ToolResult.Ok;
        }
    }
}
