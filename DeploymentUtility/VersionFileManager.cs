﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace DeploymentUtility
{
    public static class VersionFileManager
    {
        private static Regex assemblyVersionRegex = new Regex(@"\[assembly: Assembly(File)?Version\(\""\d+\.\d+\.\d+\.\d+\""\)\]");
        private static Regex productVersionRegex = new Regex(@"(?:\""ProductVersion\"" = \""8.)\d+\.\d+\.\d+\""");

        /// <summary>
        /// ファイルのバージョン情報を取得します。
        /// </summary>
        /// <param name="filePath">ファイルパス</param>
        /// <param name="fileType">ファイル種別</param>
        /// <returns>バージョン情報構造体</returns>
        public static VersionData ReadVersion(string filePath, FileType fileType)
        {
            Regex matchingRegex;
            if (fileType == FileType.DeploymentProject)
            {
                matchingRegex = productVersionRegex;
            }
            else
            {
                matchingRegex = assemblyVersionRegex;
            }

            List<VersionData> versionDataList = new List<VersionData>();
            using (StreamReader sr = new StreamReader(filePath, Encoding.UTF8))
            {
                while (true)
                {
                    string line = sr.ReadLine();
                    if (line == null)
                    {
                        break;
                    }

                    //取得する
                    if (matchingRegex.IsMatch(line))
                    {
                        versionDataList.Add(ReadVersionString(line, fileType));
                    }
                }
            }

            //取得できないと例外。assemblyinfoのみ、２つが違ったら例外
            if (((fileType == FileType.DeploymentProject) && (versionDataList.Count != 1)) || ((fileType == FileType.AssemblyInfo) && (versionDataList.Count != 2)))
            {
                throw new Exception("Read version failed.");
            }
            else if ((fileType == FileType.AssemblyInfo) && (!versionDataList[0].Equals(versionDataList[1])))
            {
                throw new Exception("Version is different between AssemblyVersion and AssemblyFileVersion.");
            }

            return versionDataList[0];
        }

        /// <summary>
        /// バージョン文字列を読み取ります。
        /// </summary>
        /// <param name="s">読み取り対象文字列</param>
        /// <param name="fileType">ファイル種別</param>
        /// <returns>バージョン情報構造体</returns>
        private static VersionData ReadVersionString(string s, FileType fileType)
        {
            string versionString = FindVersionString(s, fileType);
            string[] versionStringArr = versionString.Split(new char[] { '.' });

            VersionData versionData = new VersionData();
            versionData.Major = int.Parse(versionStringArr[0]);
            versionData.Minor = int.Parse(versionStringArr[1]);
            versionData.Build = int.Parse(versionStringArr[2]);
            versionData.Revision = (fileType == FileType.AssemblyInfo) ? int.Parse(versionStringArr[3]) : 0;

            return versionData;
        }

        /// <summary>
        /// バージョン情報を書き込みます。
        /// </summary>
        /// <param name="filePath">ファイルパス</param>
        /// <param name="fileType">ファイル種別</param>
        /// <param name="newVersion">バージョン情報構造体</param>
        public static void WriteVersion(string filePath, FileType fileType, VersionData newVersion)
        {
            List<Regex> matchingRegexList = new List<Regex>();
            if (fileType == FileType.DeploymentProject)
            {
                matchingRegexList.Add(productVersionRegex);
                matchingRegexList.Add(new Regex(@"(?:\""(?:Product|Package|Upgrade)Code\"" = \""8.){([\d\w-]+)}"));
            }
            else
            {
                matchingRegexList.Add(assemblyVersionRegex);
            }

            string tmpFilePath = filePath + ".tmp.txt";
            using (StreamReader sr = new StreamReader(filePath))
            {
                using (StreamWriter sw = new StreamWriter(tmpFilePath, false, Encoding.UTF8))
                {
                    while (true)
                    {
                        string line = sr.ReadLine();
                        if (line == null)
                        {
                            break;
                        }

                        //更新する
                        foreach (Regex regex in matchingRegexList)
                        {
                            if (regex.IsMatch(line))
                            {
                                if (regex.ToString().Contains("(?:Product|Package|Upgrade)Code"))
                                {
                                    line = UpdateGUID(line);
                                }
                                else
                                {
                                    line = UpdateVersionString(line, fileType, newVersion);
                                }

                                break;
                            }
                        }

                        sw.WriteLine(line);
                    }
                }
            }

            File.Delete(filePath);
            File.Move(tmpFilePath, filePath);
        }

        /// <summary>
        /// 入力文字列のGUIDを更新します。
        /// </summary>
        /// <param name="s">文字列</param>
        /// <returns>GUIDを更新した文字列</returns>
        private static string UpdateGUID(string s)
        {
            Regex regex = new Regex(@"{([\d\w-]+)}");
            string result = regex.Replace(s, string.Format("{{{0}}}", Guid.NewGuid().ToString().ToUpper()));
            return result;
        }

        /// <summary>
        /// バージョン情報の文字列を更新します。
        /// </summary>
        /// <param name="s">更新対象の文字列</param>
        /// <param name="fileType">ファイル種別</param>
        /// <param name="newVersion"></param>
        /// <returns></returns>
        private static string UpdateVersionString(string s, FileType fileType, VersionData newVersion)
        {
            string newVersionString;
            if (fileType == FileType.AssemblyInfo)
            {
                newVersionString = string.Format("{0}.{1}.{2}.{3}", newVersion.Major, newVersion.Minor, newVersion.Build, newVersion.Revision);
            }
            else
            {
                newVersionString = string.Format("{0}.{1}.{2}", newVersion.Major, newVersion.Minor, newVersion.Build);
            }

            string currentVersionString = FindVersionString(s, fileType);
            string result = s.Replace(currentVersionString, newVersionString);
            return result;
        }

        /// <summary>
        /// バージョン番号の文字列を抽出します。
        /// </summary>
        /// <param name="s">抽出対象の文字列</param>
        /// <param name="fileType">ファイル種別</param>
        /// <returns>バージョン番号文字列</returns>
        private static string FindVersionString(string s, FileType fileType)
        {
            int idx1, idx2;
            if (fileType == FileType.AssemblyInfo)
            {
                idx1 = s.IndexOf('"') + 1;
                idx2 = s.IndexOf('"', idx1);
            }
            else
            {
                idx1 = s.IndexOf(':') + 1;
                idx2 = s.IndexOf('"', idx1);
            }

            string versionString = s.Substring(idx1, (idx2 - idx1));
            return versionString;
        }
    }
}
