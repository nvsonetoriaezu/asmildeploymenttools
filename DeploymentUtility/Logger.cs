﻿using System;
using System.IO;
using System.Text;

namespace DeploymentLogger
{
    /// <summary>
    /// ログ出力クラスです。
    /// </summary>
    public static class Logger
    {
        /// <summary>
        /// 例外をログ出力します。
        /// </summary>
        /// <param name="logFilePath">ログファイルパス</param>
        /// <param name="ex">例外</param>
        public static void WriteException(string logFilePath, Exception ex, string caption)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(logFilePath, true, Encoding.UTF8))
                {
                    if (!string.IsNullOrWhiteSpace(caption))
                    {
                        sw.WriteLine(caption);
                    }
                    sw.WriteLine(ex.Message);
                    sw.WriteLine(string.IsNullOrWhiteSpace(ex.StackTrace) ? Environment.StackTrace : ex.StackTrace);
                    sw.WriteLine("");
                }
            }
            catch
            {
                //DO NOTHING
            }
        }
    }
}
