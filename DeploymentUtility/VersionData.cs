﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeploymentUtility
{
    /// <summary>
    /// バージョン情報の構造体です。
    /// </summary>
    public struct VersionData
    {
        /// <summary>
        /// メジャーバージョンを取得または設定します。
        /// </summary>
        public int Major;

        /// <summary>
        /// マイナーバージョンを取得または設定します。
        /// </summary>
        public int Minor;

        /// <summary>
        /// ビルドを取得または設定します。
        /// </summary>
        public int Build;

        /// <summary>
        /// リビジョンを取得または設定します。
        /// </summary>
        public int Revision;
    }
}
