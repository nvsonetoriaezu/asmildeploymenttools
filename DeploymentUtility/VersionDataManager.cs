﻿using System;

namespace DeploymentUtility
{
    /// <summary>
    /// バージョン情報管理クラスです。
    /// </summary>
    public static class VersionDataManager
    {
        /// <summary>
        /// バージョン情報のビルド番号を更新します。.
        /// </summary>
        /// <param name="currentData">現行のバージョン情報構造体</param>
        /// <returns>変更後のバージョン情報構造体</returns>
        public static VersionData UpdateBuild(VersionData currentData)
        {
            VersionData newData = new VersionData();
            newData.Major = currentData.Major;
            newData.Minor = currentData.Minor;
            newData.Build = currentData.Build + 1;
            newData.Revision = 0;

            return newData;
        }

        /// <summary>
        /// バージョンを分割した値を取得します。
        /// </summary>
        /// <param name="data">バージョン情報構造体</param>
        /// <param name="layer">取得する階層要素のenum</param>
        /// <returns>指定した階層要素の値</returns>
        public static int SplitVersion(VersionData data, VersionLayerType layer)
        {
            switch (layer)
            {
                case VersionLayerType.Major:
                    return data.Major;
                case VersionLayerType.Minor:
                    return data.Minor;
                case VersionLayerType.Build:
                    return data.Build;
                case VersionLayerType.Revision:
                    return data.Revision;
                default:
                    throw new ArgumentOutOfRangeException(string.Format("VersionLayerType {0} is not supported.", (int)layer));
            }
        }



    }
}
