﻿namespace DeploymentUtility
{
    /// <summary>
    /// ツール実行結果のenumです。
    /// </summary>
    public enum ToolResult
    {
        /// <summary>
        /// 成功
        /// </summary>
        Ok = 0,
        /// <summary>
        /// 正常終了だが成功条件を満たさない
        /// </summary>
        NotOk = 1,
        /// <summary>
        /// エラー
        /// </summary>
        Error = -1
    }

    /// <summary>
    /// バージョン情報の種別のenumです。
    /// </summary>
    public enum VersionLayerType
    {
        /// <summary>
        /// メジャーバージョン
        /// </summary>
        Major = 0,
        /// <summary>
        /// マイナーバージョン
        /// </summary>
        Minor,
        /// <summary>
        /// ビルド
        /// </summary>
        Build,
        /// <summary>
        /// リビジョン
        /// </summary>
        Revision
    }

    /// <summary>
    /// ファイル種別のenumです。
    /// </summary>
    public enum FileType
    {
        /// <summary>
        /// AssemblyInfo.cs
        /// </summary>
        AssemblyInfo,
        /// <summary>
        /// .vdprojファイル
        /// </summary>
        DeploymentProject
    }
}
