﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VersionSplitter
{
    /// <summary>
    /// コマンドライン引数の個数のenumです。
    /// </summary>
    public enum ArgumentLength
    {
        /// <summary>
        /// ログファイル不使用時の最小の引数
        /// </summary>
        WithoutLogFilePath = 2,
        /// <summary>
        /// ログファイル使用時の引数
        /// </summary>
        WithLogFilePath = 3,
    }
}
