﻿using DeploymentUtility;
using System;

namespace VersionSplitter
{
    class Program
    {
        static int Main(string[] args)
        {
            if (args.Length < (int)ArgumentLength.WithoutLogFilePath)
            {
                return (int)ToolResult.Error;
            }

            string srcFilePath = "";
            string versionLayerString = "";
            string logFilePath = "";
            try
            {
                //引数はファイルパス(AssemblyInfo限定), 取得バージョン種別, ログファイルパスの順
                srcFilePath = args[0];
                versionLayerString = args[1];
                if ((int)ArgumentLength.WithLogFilePath <= args.Length)
                {
                    logFilePath = args[2];
                }

                VersionLayerType versionLayer;
                int i;
                if (int.TryParse(versionLayerString, out i))
                {
                    versionLayer = (VersionLayerType)i;
                }
                else
                {
                    versionLayer = (VersionLayerType)Enum.Parse(typeof(VersionLayerType), versionLayerString);
                }

                VersionData data = VersionFileManager.ReadVersion(srcFilePath, FileType.AssemblyInfo);
                int result = VersionDataManager.SplitVersion(data, versionLayer);
                return result;
            }
            catch (Exception ex)
            {
                string caption = string.Format("VersionSplitter Error : FilePath={0}, VersionLayer={1}", srcFilePath, versionLayerString);
                Console.WriteLine(caption);
                Console.WriteLine(ex.Message);
                if (!string.IsNullOrWhiteSpace(logFilePath))
                {
                    DeploymentLogger.Logger.WriteException(logFilePath, ex, caption);
                }
                return (int)ToolResult.Error;
            }
        }
    }
}
