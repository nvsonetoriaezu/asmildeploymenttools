﻿using DeploymentUtility;
using System;

namespace SetupVersionWriterSnap
{
    class Program
    {
        /// <summary>
        /// プロジェクトファイルのバージョン番号を更新します。
        /// </summary>
        /// <param name="args">コマンドライン引数</param>
        /// <returns>0=成功, -1=エラー</returns>
        static int Main(string[] args)
        {
            string projectFilePath1 = "";
            string projectFilePath2 = "";
            string assemblyInfoFilePath1 = "";
            string assemblyInfoFilePath2 = "";
            string logFilePath = "";

            try
            {
                // vdproj,assemblyinfo,assemblyinfoを要求する。あとエラーログのファイルパス
                if (args.Length < (int)ArgumentLength.WithoutLogFilePath)
                {
                    return (int)ToolResult.Error;
                }

                projectFilePath1 = args[0];
                projectFilePath2 = args[1];
                assemblyInfoFilePath1 = args[2];
                assemblyInfoFilePath2 = args[3];
                if ((int)ArgumentLength.WithLogFilePath <= args.Length)
                {
                    logFilePath = args[4];
                }

                VersionData newVersion = VersionDataManager.UpdateBuild(VersionFileManager.ReadVersion(assemblyInfoFilePath1, FileType.AssemblyInfo));
                VersionFileManager.WriteVersion(projectFilePath1, FileType.DeploymentProject, newVersion);
                VersionFileManager.WriteVersion(projectFilePath2, FileType.DeploymentProject, newVersion);
                VersionFileManager.WriteVersion(assemblyInfoFilePath1, FileType.AssemblyInfo, newVersion);
                VersionFileManager.WriteVersion(assemblyInfoFilePath2, FileType.AssemblyInfo, newVersion);
            }
            catch (Exception ex)
            {
                string caption = string.Format(
                    "SetupVersionWriterSnap Error : ProjectFile1={0}, ProjectFile2={1}, AssemblyInfo1={2}, AssemblyInfo2={3}",
                    projectFilePath1,
                    projectFilePath2,
                    assemblyInfoFilePath1,
                    assemblyInfoFilePath2);
                Console.WriteLine(caption);
                Console.WriteLine(ex.Message);
                if (!string.IsNullOrWhiteSpace(logFilePath))
                {
                    DeploymentLogger.Logger.WriteException(logFilePath, ex, caption);
                }
                return (int)ToolResult.Error;
            }

            return (int)ToolResult.Ok;
        }
    }
}
