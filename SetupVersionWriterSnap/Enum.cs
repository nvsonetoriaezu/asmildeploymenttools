﻿namespace SetupVersionWriterSnap
{
    /// <summary>
    /// コマンドライン引数の個数のenumです。
    /// </summary>
    public enum ArgumentLength
    {
        /// <summary>
        /// ログファイル不使用時の最小の引数
        /// </summary>
        WithoutLogFilePath = 4,
        /// <summary>
        /// ログファイル使用時の引数
        /// </summary>
        WithLogFilePath = 5,
    }
}
