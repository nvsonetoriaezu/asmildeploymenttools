﻿using System;
using System.IO;
using FileSizeChecker;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest.TestClass.FileSizeChecker
{
    [TestClass]
    public class FileManagerTest
    {
        [TestMethod]
        public void HasFileDataTest01()
        {
            string root = Path.Combine(TestData.TestDataPath, @"FileSizeChecker");
            FileManager.HasFileData(Path.Combine(root, "1byteData.txt")).IsTrue();
            FileManager.HasFileData(Path.Combine(root, "NoData.txt")).IsFalse();

            bool hasExceptionOccured = false;
            try
            {
                FileManager.HasFileData(Path.Combine(root, "FileNotFound.txt")).IsFalse();
            }
            catch (FileNotFoundException)
            {
                hasExceptionOccured = true;
            }
            hasExceptionOccured.IsTrue();
        }
    }
}
