﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DeploymentUtility;
using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace UnitTest.TestClass.DeploymentUtility
{
    [TestClass]
    public class FileManagerTest
    {
        [TestMethod]
        public void ReadVersionTest_01()
        {
            string filePath = Path.Combine(TestData.TestDataPath, @"DeploymentUtility\AssemblyInfo_Valid.txt");
            VersionData data = VersionFileManager.ReadVersion(filePath, FileType.AssemblyInfo);
            data.Major.Is(1);
            data.Minor.Is(2);
            data.Build.Is(34);
            data.Revision.Is(5);
        }

        [TestMethod]
        public void ReadVersionTest_02()
        {
            string filePath = Path.Combine(TestData.TestDataPath, @"DeploymentUtility\AssemblyInfo_Invalid.txt");

            bool hasExceptionOccured = false;
            try
            {
                VersionData data = VersionFileManager.ReadVersion(filePath, FileType.AssemblyInfo);
            }
            catch (Exception ex)
            {
                ex.Message.Is("Version is different between AssemblyVersion and AssemblyFileVersion.");
                hasExceptionOccured = true;
            }

            hasExceptionOccured.IsTrue();
        }

        [TestMethod]
        public void ReadVersionTest_03()
        {
            string filePath = Path.Combine(TestData.TestDataPath, @"DeploymentUtility\AssemblyInfo_NoData.txt");

            bool hasExceptionOccured = false;
            try
            {
                VersionData data = VersionFileManager.ReadVersion(filePath, FileType.AssemblyInfo);
            }
            catch (Exception ex)
            {
                ex.Message.Is("Read version failed.");
                hasExceptionOccured = true;
            }

            hasExceptionOccured.IsTrue();
        }

        [TestMethod]
        public void ReadVersionTest_04()
        {
            string filePath = Path.Combine(TestData.TestDataPath, @"DeploymentUtility\vdproj_Valid.txt");
            VersionData data = VersionFileManager.ReadVersion(filePath, FileType.DeploymentProject);
            data.Major.Is(1);
            data.Minor.Is(2);
            data.Build.Is(34);
            data.Revision.Is(0);
        }

        [TestMethod]
        public void ReadVersionTest_05()
        {
            string filePath = Path.Combine(TestData.TestDataPath, @"DeploymentUtility\vdproj_NoData.txt");

            bool hasExceptionOccured = false;
            try
            {
                VersionData data = VersionFileManager.ReadVersion(filePath, FileType.DeploymentProject);
            }
            catch (Exception ex)
            {
                ex.Message.Is("Read version failed.");
                hasExceptionOccured = true;
            }

            hasExceptionOccured.IsTrue();
        }

        [TestMethod]
        public void WriteVersionTest_01()
        {
            string srcPath = Path.Combine(TestData.TestDataPath, @"DeploymentUtility\AssemblyInfo_Valid.txt");
            string filePath = srcPath.Replace(".txt", "_updated.txt");
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
            File.Copy(srcPath, filePath);

            //バージョン読み込み
            VersionData current = VersionFileManager.ReadVersion(filePath, FileType.AssemblyInfo);
            //バージョン更新
            VersionData updated = VersionDataManager.UpdateBuild(current);
            //ファイルに書き込み
            VersionFileManager.WriteVersion(filePath, FileType.AssemblyInfo, updated);

            //書き込んだバージョンの読み込み
            VersionData data = VersionFileManager.ReadVersion(filePath, FileType.AssemblyInfo);
            data.Major.Is(1);
            data.Minor.Is(2);
            data.Build.Is(35);
            data.Revision.Is(5);

            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
        }

        [TestMethod]
        public void WriteVersionTest_02()
        {
            string srcPath = Path.Combine(TestData.TestDataPath, @"DeploymentUtility\vdproj_Valid.txt");
            string filePath = srcPath.Replace(".txt", "_updated.txt");
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
            File.Copy(srcPath, filePath);

            //バージョン読み込み
            VersionData current = VersionFileManager.ReadVersion(filePath, FileType.DeploymentProject);
            //バージョン更新
            VersionData updated = VersionDataManager.UpdateBuild(current);
            //ファイルに書き込み
            VersionFileManager.WriteVersion(filePath, FileType.DeploymentProject, updated);

            //書き込んだバージョンの読み込み
            VersionData data = VersionFileManager.ReadVersion(filePath, FileType.DeploymentProject);
            data.Major.Is(1);
            data.Minor.Is(2);
            data.Build.Is(35);
            data.Revision.Is(0);

            //GUIDの更新を確認
            bool isProductCodeUpdated = false;
            bool isPackageCodeUpdated = false;
            bool isUpgradeCodeUpdated = false;
            using (StreamReader sr = new StreamReader(filePath, Encoding.UTF8))
            {
                while (true)
                {
                    string s = sr.ReadLine();
                    if (s == null)
                    {
                        break;
                    }

                    if (s.Contains("Code"))
                    {   
                        Regex regexGUID = new Regex(@"{([\d\w-]+)}");
                        regexGUID.IsMatch(s).IsTrue();
                        string matchedGUID = regexGUID.Match(s).Value;

                        if (s.Contains("Product"))
                        {
                            matchedGUID.IsNot("{00000000-0000-0000-0000-000000000000}");
                            isProductCodeUpdated = true;
                        }
                        else if (s.Contains("Package"))
                        {
                            matchedGUID.IsNot("{11111111-1111-1111-1111-111111111111}");
                            isPackageCodeUpdated = true;
                        }
                        else if (s.Contains("Upgrade"))
                        {
                            matchedGUID.IsNot("{22222222-2222-2222-2222-222222222222}");
                            isUpgradeCodeUpdated = true;
                        }
                    }
                }
            }
            isProductCodeUpdated.IsTrue();
            isPackageCodeUpdated.IsTrue();
            isUpgradeCodeUpdated.IsTrue();

            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
        }
    }
}
