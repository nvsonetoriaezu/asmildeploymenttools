﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DeploymentUtility;
using System;

namespace UnitTest.TestClass.SetupVersionWriter
{
    [TestClass]
    public class VersionDataManagerTest
    {
        [TestMethod]
        public void UpdateBuildTest()
        {
            VersionData current = new VersionData()
            {
                Major = 12,
                Minor = 34,
                Build = 56,
                Revision = 78
            };

            VersionData updated = VersionDataManager.UpdateBuild(current);
            updated.Major.Is(12);
            updated.Minor.Is(34);
            updated.Build.Is(57);
            updated.Revision.Is(0);
            current.Equals(updated).IsFalse();
        }

        [TestMethod]
        public void SplitVersionTest()
        {
            VersionData data = new VersionData()
            {
                Major = 12,
                Minor = 34,
                Build = 56,
                Revision = 78
            };

            VersionDataManager.SplitVersion(data, VersionLayerType.Major).Is(12);
            VersionDataManager.SplitVersion(data, VersionLayerType.Minor).Is(34);
            VersionDataManager.SplitVersion(data, VersionLayerType.Build).Is(56);
            VersionDataManager.SplitVersion(data, VersionLayerType.Revision).Is(78);

            int[] outOfRangeArr = new int[] { -1, 4 };
            int exceptionCount = 0;
            for (int i = 0; i < outOfRangeArr.Length; i++)
            {
                try
                {
                    VersionDataManager.SplitVersion(data, (VersionLayerType)outOfRangeArr[i]);
                }
                catch (ArgumentOutOfRangeException)
                {
                    exceptionCount++;
                }
            }
            exceptionCount.Is(outOfRangeArr.Length);
        }
    }
}
