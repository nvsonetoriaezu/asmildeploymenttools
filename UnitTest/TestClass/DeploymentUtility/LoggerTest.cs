﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using DeploymentLogger;

namespace UnitTest.TestClass.DeploymentUtility
{
    [TestClass]
    public class LoggerTest
    {
        [TestMethod]
        public void WriteExceptionTest()
        {
            string logFilePath = Path.Combine(TestData.TestDataPath, @"DeploymentUtility\log.txt");
            if (File.Exists(logFilePath))
            {
                File.Delete(logFilePath);
            }

            string msg01 = "Test Exception Message 01";
            string msg02 = "Test Exception Message 02";
            string caption01 = "Caption01";
            string caption02 = "Caption01";
            Logger.WriteException(logFilePath, new Exception(msg01), caption01);
            Logger.WriteException(logFilePath, new Exception(msg02), caption02);

            bool hasWritten01 = false;
            bool hasWritten02 = false;
            using (StreamReader sr = new StreamReader(logFilePath, System.Text.Encoding.UTF8))
            {
                string s = sr.ReadToEnd();
                if (s.Contains(msg01) && s.Contains(caption01))
                {
                    hasWritten01 = true;
                }

                if (s.Contains(msg02) && s.Contains(caption02))
                {
                    hasWritten02 = true;
                }
            }

            File.Delete(logFilePath);
            hasWritten01.IsTrue();
            hasWritten02.IsTrue();
        }
    }
}
