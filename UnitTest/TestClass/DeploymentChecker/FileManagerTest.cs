﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DeploymentChecker;
using System.IO;

namespace UnitTest.TestClass.DeploymentChecker
{
    [TestClass]
    public class FileManagerTest
    {
        [TestMethod]
        public void FindDeployedFilesTest_01()
        {
            string root = Path.Combine(TestData.TestDataPath, @"DeploymentChecker\FileManager\01");
            string[] files = new string[]
            {
                "AsmilVisionSetup.msi",
                "setup.exe",
                "silent_install.bat",
                "silent_uninstall.bat"
            };

            //ファイル構成が完全なディレクトリを検索
            bool result = FileManager.FindDeployedFiles(root, DateTime.MinValue, files);
            result.IsTrue();
            //ファイル構成が完全なディレクトリを検索（StartDate指定より古いディレクトリを除外）
            result = FileManager.FindDeployedFiles(root, DateTime.MaxValue, files);
            result.IsFalse();
        }

        [TestMethod]
        public void FindDeployedFilesTest_02()
        {
            string root = Path.Combine(TestData.TestDataPath, @"DeploymentChecker\FileManager\02");
            string[] files = new string[]
            {
                "AsmilVisionSetup.msi",
                "setup.exe",
                "silent_install.bat",
                "silent_uninstall.bat"
            };

            //ファイル構成が不完全なディレクトリを検索
            bool result = FileManager.FindDeployedFiles(root, DateTime.MinValue, files);
            result.IsFalse();
        }

        [TestMethod]
        public void FindDeployedFilesTest_03()
        {
            string root = Path.Combine(TestData.TestDataPath, @"DeploymentChecker\FileManager\03");
            string[] files = new string[]
            {
                "AsmilVisionSetup.msi",
                "setup.exe",
                "silent_install.bat",
                "silent_uninstall.bat"
            };

            //ファイルがないディレクトリを検索
            bool result = FileManager.FindDeployedFiles(root, DateTime.MinValue, files);
            result.IsFalse();
        }
    }
}
