﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace UnitTest
{
    public class TestData
    {
        public static string AssemblyPath { get { return System.Reflection.Assembly.GetExecutingAssembly().Location; } }
        public static string TestDataPath { get { return Path.Combine(AssemblyPath, @"..\..\..\TestData"); } }

    }
}
